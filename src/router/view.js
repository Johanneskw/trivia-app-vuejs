import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Trivia from '@/components/Trivia'
import Final from '@/components/Final'



Vue.use(Router);

export default new Router({
    //Setting routes
    routes: [
        {
            path: '/',
            name: 'welcome',
            component: Welcome
        },
        {
            path: '/trivia',
            name: 'trivia',
            component: Trivia
        },
        {
            path: '/final',
            name: 'final',
            component: Final
        }

    ]
})
